from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from listenup_common.utils import guid
import enum

db = SQLAlchemy()


class QualityEnum(enum.Enum):
    HIGH = 10
    MEDIUM = 5
    LOW = 1


class BaseModel(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    creation_timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


class Image(BaseModel):
    __tablename_ = 'image'
    guid = db.Column(db.String(32), nullable=False)
    alt_text = db.Column(db.String(400), nullable=True)
    custom_url = db.Column(db.String(100), nullable=True)

    def __init__(self):
        self.guid = guid()

    def to_dict(self, include_files=True):
        item_dict = {
            'id': self.id,
            'creation_timestamp': self.creation_timestamp,
            'guid': self.guid,
            'alt_text': self.alt_text,
            'custom_url': self.custom_url,
        }

        if include_files:
            owners = ImageFile.query.filter_by(image_guid=self.guid).all()
            item_dict['files'] = [owner.to_dict() for owner in owners]

        return item_dict


class ImageFile(BaseModel):
    __tablename__ = 'image_file'
    guid = db.Column(db.String(32), nullable=False)
    image_guid = db.Column(db.String(32), nullable=False)
    image_quality = db.Column(db.Enum(QualityEnum), nullable=False)
    file_guid = db.Column(db.String(32), nullable=False)
    image_width = db.Column(db.Integer)
    image_height = db.Column(db.Integer)
    image_type = db.Column(db.String(12))

    def __init__(self, image: Image):
        self.guid = guid()
        self.image_guid = image.guid

    def to_dict(self):
        return {
            'id': self.id,
            'creation_timestamp': self.creation_timestamp,
            'guid': self.guid,
            'image_guid': self.image_guid,
            'file_guid': self.file_guid,
            'quality': self.image_quality,
            'width': self.image_width,
            'height': self.image_height,
            'type': self.image_type,
        }
