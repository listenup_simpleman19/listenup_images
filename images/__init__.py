import os
import sys
from images.models import db
from flask import Flask, request, session
from flask_migrate import Migrate
from celery import Celery
from config import config

from images import models

migrate = Migrate()

celery_broker = 'redis://:' + os.environ.get('REDIS_PASSWORD') + '@' + os.environ.get('REDIS') + '/2'


# TODO move to common
def start_debug(port):
    sys.path.append("pycharm-debug.egg")
    sys.path.append("pycharm-debug-py3k.egg")

    import pydevd
    pydevd.settrace('192.168.33.1', port=int(port), stdoutToServer=True, stderrToServer=True)


# TODO remove this it will print the password in plain text...
print(celery_broker)

celery = Celery(__name__,
                broker=celery_broker,
                backend=celery_broker)
celery.config_from_object('celeryconfig')

if os.environ.get("DEBUG_PORT", None):
    start_debug(os.environ.get("DEBUG_PORT"))

from images import tasks


def create_app(config_name=None):
    if config_name is None:
        config_name = os.environ.get('LISTENUP_APP_CONFIG', 'development')
        print("Starting up in: " + config_name)
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    app.host = '0.0.0.0'

    celery.conf.update(config[config_name].CELERY_CONFIG)

    # Initialize flask extensions
    db.init_app(app)
    migrate.init_app(app=app, db=db)

    # Register web application routes
    from .routes import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app
