import os
from flask import Blueprint, render_template, session, redirect, url_for, request, current_app as app, g

from listenup_common.api_response import ApiException, ApiMessage, ApiResult
from images.tasks import test_task
from images.models import Image, ImageFile
from . import db

main = Blueprint('main', __name__, url_prefix='/api/images')

root_path = os.path.dirname(os.path.abspath(__file__))


@main.route("/test")
def test():
    test_task.apply_async(args=[], countdown=15)
    return ApiResult(value={"hi": "this is a test"}, status=200).to_response()


@main.route("/add", methods=["POST"])
def add_image():
    if request.json:
        content = request.json
        image = Image()
        image.title = content.get('alt_text', None)
        db.session.add(image)

        db.session.commit()

        return ApiResult(value=image.to_dict(include_files=True), status=200).to_response()
    else:
        raise ApiException(message="Request was not json", status=400)


@main.route("/all")
def get_image_all():
    images = Image.query.all()
    return ApiResult(value=[image.to_dict(include_files=True) for image in images], status=200).to_response()


@main.route("/<string:guid>")
def get_image(guid):
    image = Image.query.filter_by(guid=guid).one_or_none()
    if image:
        return ApiResult(value=image.to_dict(include_files=True), status=200).to_response()
    else:
        raise ApiException(message="Could not find image for " + guid, status=404)


@main.errorhandler(ApiException)
def handle_api_exception(error: ApiException):
    return error.to_result().to_response()
