#!/usr/bin/env bash


TARGET="$(python -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")/celery/backends"
pushd $TARGET
    if [ -e async.py ]
    then
        echo "Fixing async keyword for python 3.7 - remove this when fixed by celery project"
        mv async.py asynchronous.py
        sed -i 's/async/asynchronous/g' redis.py
        sed -i 's/async/asynchronous/g' rpc.py
    fi
popd
